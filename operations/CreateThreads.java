package operations;
import des.*;
public class CreateThreads implements Runnable{
	Thread t;
	volatile boolean alive;
	/*
		This variable is made as voltile because the execution of all the Threads depends on this variable!!!
		If the processor caches its value and take the wrong value of this boolean then there is problem.
		So, by making it volatile we ensure that the core will not caches its value and thereby no problem!!!
	*/
	int indexNo;
	String message;
	int threadArrayIndexNo;
	boolean isReverse,isReassign;
	CreateThreads(int indexNo,String message,int threadArrayIndexNo,boolean isReverse,boolean isReassign){
		this.indexNo = indexNo;
		this.message = message;
		this.isReverse = isReverse;
		this.isReassign = isReassign;
		this.threadArrayIndexNo = threadArrayIndexNo;//Thread is of which number !!!
		t = new Thread(this);
		this.alive = true;
		t.start();
	}
	public void run(){
		threadWorking();
	}

	void threadWorking(){
		String messageInBits = DESWorking.convertIntoBits(message,true);
        //Step2 Begins!!!(i.e Encoding the 64 bit block of the data)
        String messageIntoIP = DESWorking.permutationOfMatrix(CipherSpecification.ip,messageInBits);
        String l0 = messageIntoIP.substring(0,32);
        String r0 = messageIntoIP.substring(32,64);
        
        DESWorking.finalStringArray[indexNo] = DESWorking.initailizeL16AndR16(l0,r0,isReverse);
        if(this.isReassign == true)
	        DESWorking.reassigningTheThread(threadArrayIndexNo,isReverse,this);
    }
    public void setAlive(boolean alive){
    	this.alive = alive;
    }
}