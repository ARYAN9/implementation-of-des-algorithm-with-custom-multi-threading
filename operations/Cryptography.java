package operations;
public interface Cryptography {
    String encrypt(String message);
	String decrypt(String message);
}
